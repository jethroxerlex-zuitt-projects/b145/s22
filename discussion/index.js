//JSON OBJECTS
// -JavaScript Object Notation (JSON)
//JSON is a data format used by Javascript as well as other programming languages
//JSON objects are NOT to be confused with JavaSCript Objects
//Use of double quotation marks is required in JSON Objects
//Syntax:
/*
Syntax:
	{
	"propertyA":"valueA",
	"propertyB":"valueB"
	}
*/

// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// {
// 	"number": 1
// 	"boolean": true
// }

//JavaScript Array of Objects:

let myArr = [
{name: "Jino"},
{name: "John"}
];

//Array of JSON objects
// {
// 	"cities": [
// 	{"city": "Quezon City"},
// 	{"city": "Makati City"}
// 	]
// }

//Converting JS data into Strinified JSON
//- Stringified JSON is a JavaScript Object converted into a string to be used by the receiving application back-end application or other functions of a JavaScript application

let batchesArr = [
	{batchname: 'Batch 145'},
	{batchname: 'Batch 146'},
	{batchname: 'Batch 147'}
]

let stringifiedData = JSON.stringify(batchesArr);
console.log(stringifiedData);

//[{"batchname":"Batch 145"},{"batchname":"Batch 146"},{"batchname":"Batch 147"}] -result

//Converting Stringified JSON into Javascript Objects

let fixedData = JSON.parse(stringifiedData);
console.log(fixedData);